package main

import (
	"os"
	"github.com/eclipse/paho.mqtt.golang"
	"time"
	"crypto/tls"
	"log"
	"syscall"
	"os/signal"
	"gitlab.com/matchx/iota-stream/data"
	"math/rand"
	"encoding/hex"
)

func main() {
	opt := mqtt.NewClientOptions()
	opt.Username = "user"
	opt.Password = "12345"
	opt.ClientID = "client"
	opt.CleanSession = true
	opt.MaxReconnectInterval = time.Second
	opt.SetKeepAlive(30 * time.Second)
	opt.TLSConfig = tls.Config{
		InsecureSkipVerify: true,
		ClientAuth:         tls.NoClientCert,
	}
	opt.AddBroker("tcp://127.0.0.1:1883")
	opt.OnConnect = func(c mqtt.Client) {
		time.Sleep(1 * time.Second)
		payload, _ := hex.DecodeString("21dd13021bc0")
		log.Println(payload)
		for {
			select {
			case <-time.After(500 * time.Millisecond):
				rand.Seed(time.Now().UnixNano())
				payload[4] =   byte(int8(rand.Intn(30) -10))
				devEui := [8]byte{}
				devEuiData, _ := hex.DecodeString("78AF58FFFE040001")
				copy(devEui[:], devEuiData[:])

				p := &data.Packet{
					Time:   time.Now(),
					DevEUI: devEui,
					RXInfo: []data.RxInfo{
						{MAC: [8]byte{0, 1, 0, 1, 0, 1, 0, 1}},
					},
					ApplicationID: "1",
					Data:          payload[:],
				}
				d, _ := data.Encode(p)
				log.Println("Publish", p)
				c.Publish("application/1/node/1/rx", 0, false, d)
				break
			}
		}
	}
	client := mqtt.NewClient(opt)
	log.Println("client", client)

	go func() {
		time.Sleep(3 * time.Second)
		os.Exit(1)
	}()

	tok := client.Connect()
	tok.Wait()
	if err := tok.Error(); err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)
	<-sigChan
}
