const fetch = require('node-fetch');
const crypto = require('crypto');
const Mam = require('./mam.node.js');
const IOTA = require('iota.lib.js');
const iota = new IOTA({provider: 'https://testnet140.tangle.works'});
const mqtt = require('mqtt');
const mqttClient = mqtt.connect('tcp://127.0.0.1:1883', {username: "packet_saver", password: "blabla", clientId: "iota-stream", rejectUnauthorized: false})

const {Pool, Client} = require('pg');
var _ = require('lodash/core');

let endpoint = 'https://api.marketplace.tangle.works/newData';

// Random Key Generator
const keyGen = length => {
    const charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ9';
    const values = crypto.randomBytes(length);
    const result = new Array(length);
    for (let i = 0; i < length; i++) {
        result[i] = charset[values[i] % charset.length]
    }
    return result.join('')
};

const garageState = '';
const connected = false;
// Initialise MAM State
let mamState = Mam.init(iota, keyGen(81));
let mamKey = keyGen(81);// Set initial key

// Set Varibles
let debug = true; // Set to 'false' to publish data live
// let uuid = 'test-device-1' // Your device ID is here.

function decodeData(b) {
    if (b[0] === 0x21) {
        b = b.slice(2);
    }
    if ((b[0] & 0x10) === 0x10) {
        let size = b[0] ^ 0x10;
        if (b[1] === 0x02) {

            return b.slice(1, 3)
        }
    }
    return null;
}


// Publish to tangle
const loadDevices = async () => {
    const client = new Client({
        user: 'loraserver_as',
        host: 'localhost',
        database: 'loraserver_as',
        password: 'loraserver_as',
        port: 5432,
    });
    client.connect();

    const query = {
        text: 'select dev_eui, application_id, name, iota, iota_sk from node where iota_sk <> \'\'',
    };
    let promise = new Promise((resolve, reject) => {
        client.query(query, (err, res) => {
            if (err) {
                reject(err)
            } else {
                resolve(_.map(res.rows, (r) => {
                    r.devEui = r.dev_eui.toString('hex');
                    return r
                }))
            }
        });
    });
    return promise;
};


mqttClient.on('connect', () => {
    mqttClient.subscribe('application/+/node/+/rx')
});

const connectMqtt = async () => {
    let devices = await loadDevices();

    mqttClient.on('message', (topic, message) => {
        let packet = JSON.parse(message);
        let data = decodeData(Buffer.from(packet.data, 'base64'));

        if (data !== undefined && data.length === 2 && data[0] === 2) {
            let device = _.find(devices, function (d) {
                return d.devEui === packet.devEUI;
            });

            if (device) {
                publish({
                    time: Date.now(),
                    data: {
                        temp: data.readInt8(1),
                    }
                }, device.name, device.iota_sk)
            }
        }
    });
};

connectMqtt().then();

// Publish to tangle
const publish = async (packet, deviceName, sk) => {
    console.log("Send packet: ", packet, deviceName, sk)
    // Set channel mode & update key
    mamState = Mam.changeMode(mamState, 'restricted', mamKey);
    // Create Trytes
    const trytes = iota.utils.toTrytes(JSON.stringify(packet))
    /* Get MAM payload*/
    const message = Mam.create(mamState, trytes);
    // Save new mamState
    mamState = message.state;
    // Attach the payload.
    await Mam.attach(message.payload, message.address);
    console.log('Attached Message');
    if (!debug) {
        // Push the MAM root to the demo DB
        let pushToDemo = await pushKeys(message.root, mamKey, deviceName, sk);
        console.log(pushToDemo);
        // Change MAM key on each loop
        mamKey = keyGen(81)
    }
};

// Push keys to market place.
const pushKeys = async (root, sidekey, deviceName, sk) => {
    const packet = {
        sidekey: sidekey,
        root: root,
        time: Date.now()
    };
    // Initiate Fetch Call
    const resp = await fetch(endpoint, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({id: deviceName, packet, sk: sk})
    });
    return resp.json()
};




