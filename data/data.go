package data

import (
	"database/sql/driver"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"fmt"
	"time"
	"github.com/gin-gonic/gin/json"
)

type (
	RxInfo struct {
		MAC  eui64   `json:"mac"`
		RSSI int     `json:"rssi"`
		SNR  float64 `json:"loRaSNR"`
	}

	DataRate struct {
		Modulation   string `json:"modulation"`
		Bandwidth    int    `json:"bandwidth"`
		SpreadFactor int    `json:"spreadFactor"`
	}

	TxInfo struct {
		Frequency int      `json:"frequency"`
		DataRate  DataRate `json:"dataRate"`
		ADR       bool     `json:"adr"`
		CodeRate  string   `json:"codeRate"`
	}

	Packet struct {
		Time           time.Time
		ApplicationID  string   `json:"applicationID"`
		DevEUI         eui64    `json:"devEUI"`
		RXInfo         []RxInfo `json:"rxInfo"`
		TXInfo         TxInfo   `json:"txInfo"`
		FCnt           int      `json:"fCnt"`
		FPort          int      `json:"fPort"`
		Data           []byte   `json:"data"`
		BatteryVoltage *int
		Location       *point
		Altitude       *int
		SensorType     *int
		SensorValue    *int
	}
)

type eui64 [8]byte

func (e *eui64) UnmarshalJSON(b []byte) error {
	n, err := hex.Decode(e[:], b[1:len(b)-1])
	if n != len(e) {
		err = errors.New("wrong length")
	}
	return err
}

func (e *eui64) MarshalJSON() (res []byte, err error) {
	return []byte(fmt.Sprintf(`"%s"`, hex.EncodeToString(e[:]))), err
}

func Encode(value interface{}) (res []byte, err error) {
	res, err = json.Marshal(value)
	return
}

func (e eui64) String() string {
	return hex.EncodeToString(e[:])
}

type point struct {
	Lat string
	Lon string
}

func (p *point) Value() (driver.Value, error) {
	if p == nil {
		return nil, nil
	}
	return fmt.Sprintf("( %s, %s )", p.Lat, p.Lon), nil
}

const (
	infoParam      = iota
	infoSensorData
	infoBattery
)

type chunk struct {
	Cmd  byte
	Data []byte
}

func DecodeData(b []byte) []chunk {
	var cc []chunk
	for len(b) != 0 {
		var (
			c    chunk
			clen byte
		)
		c.Cmd = b[0] >> 4
		if clen = b[0] & 0x0f; clen == 0x0f {
			if len(b) < 2 {
				break
			}
			b = b[1:]
			clen = b[0] & 0x3f
		}
		b = b[1:]
		if len(b) < int(clen) {
			break
		}
		c.Data = b[:clen]
		b = b[clen:]
		cc = append(cc, c)
	}
	return cc
}

const (
	sensorUnknown = iota
	sensorGPS
	sensorTemp
)

func getLatLon(b []byte) string {
	i := int32(binary.LittleEndian.Uint32(b))
	return fmt.Sprint(float64(i) / 600000)
}

func getAlt(b []byte) *int {
	i := int(int16(binary.LittleEndian.Uint16(b)))
	return &i
}

func (p *Packet) SetData(c []chunk) {
	for _, v := range c {
		switch v.Cmd {
		case infoSensorData:
			if len(v.Data) == 0 {
				break
			}
			typ := v.Data[0]
			data := v.Data[1:]
			switch typ {
			case sensorGPS:
				switch len(data) {
				case 11:
					p.Altitude = getAlt(data[9:11])
					fallthrough
				case 9:
					p.Location = &point{}
					p.Location.Lat = getLatLon(data[1:5])
					p.Location.Lon = getLatLon(data[5:9])
				case 1:
					ityp := int(typ)
					ival := int(data[0])
					p.SensorType = &ityp
					p.SensorValue = &ival
				}
			case sensorTemp:
				if len(data) == 1 {
					ityp := int(typ)
					temp := int(int8(data[0]))
					p.SensorType = &ityp
					p.SensorValue = &temp
				}
			}
		case infoBattery:
			if len(v.Data) != 1 {
				break
			}
			mV := int(v.Data[0])*10 + 2000
			p.BatteryVoltage = &mV
		}
	}
}
